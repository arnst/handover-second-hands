#include "Planner.h"

#include <thread>

#include <VirtualRobot/XML/ModelIO.h>
#include <VirtualRobot/Model/LinkSet.h>
#include <VirtualRobot/CollisionDetection/CollisionChecker.h>
#include <VirtualRobot/IK/GenericIKSolver.h>
#include <VirtualRobot/Grasping/GraspSet.h>

namespace Handover
{
    Planner::Planner()
    {
        srand(time(NULL));
    }

    VirtualRobot::WorkspaceRepresentationPtr Planner::plan(bool verbose)
    {
        if (!validatePlanningData(data, true))
        {
            VR_WARNING << "Invalid Planning Data. Aborting." << std::endl;
        }

        VirtualRobot::WorkspaceRepresentationPtr iws(new VirtualRobot::WorkspaceRepresentation(data.giver));
        float minB[6];
        float maxB[6];
        for (int i = 0; i < 6; i++)
        {
            minB[i] = data.giverWorkspace->getMinBound(i);
            maxB[i] = data.giverWorkspace->getMaxBound(i);
        }
        iws->initialize(data.giverWorkspace->getJointSet(), data.discrStepTrans, data.discrStepRot, minB, maxB, data.giverWorkspace->getCollisionModelStatic(),
                        data.giverWorkspace->getCollisionModelDynamic(), data.giverWorkspace->getBaseNode(), data.giverWorkspace->getTCP(), data.giverWorkspace->getAdjustOnOverflow());

        float range = 1.0f / data.numThreads;
        std::mutex mutex;
        std::thread threads[data.numThreads];
        for (int i = 0; i < data.numThreads; i++)
        {
            std::pair<float, float> workload(i * range, (i+1) * range);
            threads[i] = std::thread([=,&mutex,&iws] () {fillInteractionWorkspace(iws, clonedReceiversPerThread[i], measuresPerThread[i], workload, mutex, i, verbose);});
        }
        for (int i = 0; i < data.numThreads; i++)
        {
            threads[i].join();
        }
        return iws;
    }

    void Planner::setPlanningData(const Planner::PlanningData& pData)
    {
        data = pData;
        if (!validatePlanningData(data, true)) return;

        // load a receiver model and a quality measure for each receiver workspace per thread
        clonedReceiversPerThread.clear();
        measuresPerThread.clear();
        for (int i = 0; i < data.numThreads; ++i)
        {
            clonedReceiversPerThread.emplace_back(std::vector<VirtualRobot::ModelPtr>());
            measuresPerThread.emplace_back(std::vector<PoseQualityHandoverPtr>());
            for (auto&& ws : data.receiverWorkspaces)
            {
                VirtualRobot::ModelPtr clonedReceiver = VirtualRobot::ModelIO::loadModel(data.receiver->getFilename(), VirtualRobot::ModelIO::eCollisionModel);
                clonedReceiver->setGlobalPose(data.receiver->getGlobalPose());
                clonedReceiver->setUpdateVisualization(false);
                clonedReceiver->setUpdateCollisionModel(false);
                clonedReceiversPerThread[i].push_back(clonedReceiver);

                PoseQualityHandoverPtr measure(new PoseQualityHandover(clonedReceiver->getJointSet(ws->getJointSet()->getName())));
                measuresPerThread[i].push_back(measure);
            }
        }

    }

    void Planner::fillInteractionWorkspace(VirtualRobot::WorkspaceRepresentationPtr &iws, std::vector<VirtualRobot::ModelPtr> &clonedReceivers,
                                           std::vector<PoseQualityHandoverPtr> &measures, const std::pair<float, float> &workload, std::mutex &mutex, int threadID, bool verbose)
    {
        VR_ASSERT(clonedReceivers.size() == measures.size());
        VR_ASSERT(iws);

        VirtualRobot::WorkspaceRepresentationPtr giverWorkspace = data.giverWorkspace;
        std::vector<VirtualRobot::WorkspaceRepresentationPtr> receiverWorkspaces = data.receiverWorkspaces;
        VirtualRobot::ManipulationObjectPtr object = data.object;

        Sphere sphereObject = getBoundingSphere(object);
        Sphere sphereWsReceiver[receiverWorkspaces.size()];
        for (int i = 0; i < receiverWorkspaces.size(); i++)
        {
            sphereWsReceiver[i] = getBoundingSphere(receiverWorkspaces[i]);
        }

        unsigned int voxelFillCount = 0;
        unsigned int voxelSkipCount = 0;
        int zStart = workload.first * iws->getNumVoxels(2);
        int zEnd = workload.second * iws->getNumVoxels(2);
        bool caching = true;

        // iterate through the freshly initialized interaction workspace
        for (unsigned int x = 0; x < iws->getNumVoxels(0); x++) {
            for (unsigned int y = 0; y < iws->getNumVoxels(1); y++) {
                for (unsigned int z = zStart; z < zEnd; z++) {

                    // Before iterating through all orientations of the current voxel, we first check if the object is in range for the receiver.
                    // First, we set the position of the objects bounding sphere to the current voxels global position (not pose)...
                    unsigned int v[6] = {x, y, z};
                    sphereObject.pos = iws->getPoseFromVoxel(v).block<3,1>(0,3);
                    // ...then we check, if the objects bounding sphere touches one of the bounding spheres of the receiver's workspaces.
                    bool reachable[receiverWorkspaces.size()];
                    bool notReachable = true;
                    for (int i = 0; i < receiverWorkspaces.size(); i++)
                    {
                        float distSquared = (sphereObject.pos - sphereWsReceiver[i].pos).squaredNorm();
                        float radiusSum = sphereObject.r + sphereWsReceiver[i].r;
                        if (distSquared <= (radiusSum * radiusSum))
                        {
                            reachable[i] = true;
                            notReachable = false;
                        } else
                        {
                            reachable[i] = false;
                        }
                    }

                    if (notReachable)
                    {
                        // At this point the object is not in range for the receiver. We don't bother iterating through all orientations for the current voxel.
                        voxelSkipCount+= receiverWorkspaces.size();
                        continue;
                    }

                    for (unsigned int alpha = 0; alpha < iws->getNumVoxels(3); alpha++) {
                        for (unsigned int beta = 0; beta < iws->getNumVoxels(4); beta++) {
                            for (unsigned int gamma = 0; gamma < iws->getNumVoxels(5); gamma++) {

                                // get the global pose of the current voxel
                                unsigned int currentVoxel[6] = {x, y, z, alpha, beta, gamma};
                                Eigen::Matrix4f giverGraspingPoseGlobal = iws->getPoseFromVoxel(currentVoxel);

                                if (giverWorkspace->isCovered(giverGraspingPoseGlobal))
                                {
                                    // we try several grasp samples
                                    unsigned char maxEntry = 0;
                                    for (int rgs = 0; rgs < data.receiverGraspSamples; rgs++)
                                    {
                                        for (std::size_t i = 0; i < receiverWorkspaces.size(); i++)
                                        {
                                            if (!reachable[i])
                                            {
                                                // At this point the object is not in range for the current receiver workspace.
                                                voxelSkipCount++;
                                                continue;
                                            }

                                            Eigen::Matrix4f receiverGraspingPoseGlobal = sampleReceiverGraspingPoseGlobal(giverGraspingPoseGlobal, object, receiverWorkspaces[i], clonedReceivers[i]);
                                            unsigned char entry = receiverWorkspaces[i]->getEntry(receiverGraspingPoseGlobal);
                                            if (entry > 0)
                                            {
                                                VirtualRobot::JointSetPtr js = clonedReceivers[i]->getJointSet(receiverWorkspaces[i]->getJointSet()->getName());
                                                bool success = solveIK(js, receiverGraspingPoseGlobal);

                                                if (success)
                                                {
                                                    VirtualRobot::LinkSetPtr staticColModel = clonedReceivers[i]->getLinkSet(receiverWorkspaces[i]->getCollisionModelStatic()->getName());
                                                    VirtualRobot::LinkSetPtr dynamicColModel = clonedReceivers[i]->getLinkSet(receiverWorkspaces[i]->getCollisionModelDynamic()->getName());
                                                    VirtualRobot::FramePtr tcp = clonedReceivers[i]->getFrame(receiverWorkspaces[i]->getTCP()->getName());
                                                    measures[i]->setObstacleDistanceVector(getObstacleDistanceVector(staticColModel, dynamicColModel, tcp));
                                                    unsigned char onlineQuality = (unsigned char)(measures[i]->getPoseQuality() * (float)UCHAR_MAX + 0.5f);
                                                    entry = ComputeQualityValue(entry, onlineQuality, 0.25f, 0.75f);
                                                    // we now weight the overall quality value with the visibility of the current voxel for the receiver.
                                                    entry *= getVisibility(receiverGraspingPoseGlobal.block<3,1>(0,3));
                                                    entry = std::max(entry, (unsigned char) 1);
                                                }
                                                else
                                                {
                                                    // here, the ik failed, but we still give the current voxel the minimum quality value instead of 0.
                                                    entry = 1;
                                                }
                                                maxEntry = std::max(maxEntry, entry);
                                            }
                                        }
                                    }
                                    if (maxEntry > 0)
                                    {
                                        mutex.lock();
                                        if (iws->getVoxelEntry(x, y, z, alpha, beta, gamma) < maxEntry) {
                                            iws->setVoxelEntry(currentVoxel, maxEntry);
                                            voxelFillCount++;
                                        }
                                        mutex.unlock();
                                    }
                                }
                            } // gamma
                        } // beta
                    } // alpha
                } // z
            } // y
        } // x

        if (verbose)
        {
            mutex.lock();
            std::cout << "[Planner][Thread " << threadID << "] Filled " << voxelFillCount << " Voxel. Skipped " << voxelSkipCount << " Voxel." << std::endl;
            mutex.unlock();
        }
    }

    bool Planner::validatePlanningData(PlanningData &data, bool spam)
    {
        if (!data.giver)
        {
            if (spam) VR_WARNING << "Giver is null." << std::endl;
            return false;
        }
        if (!data.receiver)
        {
            if (spam) VR_WARNING << "Receiver is null." << std::endl;
            return false;
        }
        if (!data.object)
        {
            if (spam) VR_WARNING << "Object is null." << std::endl;
            return false;
        }
        if (!data.giverGrasp)
        {
            if (spam) VR_WARNING << "Giver grasp is null." << std::endl;
            return false;
        }
        if (!data.giverWorkspace || !data.giverWorkspace->getData())
        {
            if (spam) VR_WARNING << "GiverWorkspace is not initialized." << std::endl;
            return false;
        }
        if (data.receiverWorkspaces.empty())
        {
            if (spam) VR_WARNING << "No ReceiverWorkspaces provided." << std::endl;
            return false;
        }
        for (const auto &ws : data.receiverWorkspaces)
        {
            if (!ws || !ws->getData())
            {
                if (spam) VR_WARNING << "A ReceiverWorkspace is not initialized." << std::endl;
                return false;
            }
            if (!ws->getCollisionModelStatic() || !ws->getCollisionModelDynamic())
            {
                if (spam) VR_WARNING << "A ReceiverWorkspace has no Collision Model assigned (which is needed)." << std::endl;
                return false;
            }
        }
        if (!data.measure)
        {
            data.measure.reset(new PoseQualityHandover(data.giverWorkspace->getJointSet()));
        }
        if (data.discrStepTrans == 0)
        {
            data.discrStepTrans = data.giverWorkspace->getDiscretizeParameterTranslation();
        }
        if (data.discrStepRot == 0)
        {
            data.discrStepRot = data.giverWorkspace->getDiscretizeParameterRotation();
        }
        if (data.receiverGraspSamples < 1)
        {
            data.receiverGraspSamples = 1;
        }
        if (data.numThreads < 1)
        {
            data.numThreads = 1;
        }

        return true;
    }

    unsigned char Planner::ComputeQualityValue(unsigned char offlineEntry, unsigned char onlineEntry, float w1, float w2) const
    {
        if (offlineEntry == 0 || onlineEntry == 0) {
            return 0;
        }

        int costOfflineEntry = UCHAR_MAX - offlineEntry;
        int costOnlineEntry = UCHAR_MAX - onlineEntry;
        int cost = w1* costOfflineEntry + w2 *costOnlineEntry;

        int qualityValue = UCHAR_MAX - cost;
        return qualityValue;
    }

    Eigen::Vector3f Planner::getObstacleDistanceVector(const VirtualRobot::LinkSetPtr &staticColModel, const VirtualRobot::LinkSetPtr &dynamicColModel, const VirtualRobot::FramePtr &tcp) const
    {
        Eigen::Vector3f minDistVector = Eigen::Vector3f::Zero();
        if (!staticColModel || !dynamicColModel || !tcp)
        {
            return minDistVector;
        }

        int id1;
        int id2;
        Eigen::Vector3f p1;
        Eigen::Vector3f p2;
        float d = staticColModel->getCollisionChecker()->calculateDistance(staticColModel, dynamicColModel, p1, p2, &id1, &id2);
        Eigen::Matrix4f obstDistPos1 = Eigen::Matrix4f::Identity();
        Eigen::Matrix4f obstDistPos2 = Eigen::Matrix4f::Identity();
        obstDistPos1.block(0, 3, 3, 1) = p1;
        obstDistPos2.block(0, 3, 3, 1) = p2;

        // transform to tcp
        Eigen::Matrix4f p1_tcp = tcp->toLocalCoordinateSystem(obstDistPos1);
        Eigen::Matrix4f p2_tcp = tcp->toLocalCoordinateSystem(obstDistPos2);
        minDistVector = p1_tcp.block(0, 3, 3, 1) - p2_tcp.block(0, 3, 3, 1);

        return minDistVector;
    }

    bool Planner::solveIK(const VirtualRobot::JointSetPtr& js, const Eigen::Matrix4f& targetPoseGlobal)
    {
        auto jointSet = js;
        VirtualRobot::GenericIKSolverPtr ikSolver(new VirtualRobot::GenericIKSolver(jointSet));
        ikSolver->setupJacobian(0.2f, 50);
        ikSolver->setMaximumError(data.discrStepTrans, data.discrStepRot);
        return ikSolver->solve(targetPoseGlobal, VirtualRobot::IKSolver::All, 4);
    }

    float Planner::getVisibility(const Eigen::Vector3f& globalPos) const
    {
        if (data.receiverGazeOriginGlobal.isZero() || data.receiverGazeDirection.isZero())
        {
            return 1.0f;
        }

        float visibility = 0.0f;

        Eigen::Vector3f faceToVoxel = globalPos - data.receiverGazeOriginGlobal;
        float cosAlpha = (faceToVoxel.dot(data.receiverGazeDirection)) / (faceToVoxel.norm() * data.receiverGazeDirection.norm());
        visibility = std::max(cosAlpha, 0.0f);

        return visibility;
    }

    Planner::Sphere Planner::getBoundingSphere(const VirtualRobot::WorkspaceRepresentationPtr& ws) const
    {
        float minBounds[3];
        float maxBounds[3];
        float maxDist = 0;
        Eigen::Vector3f pos;

        // get the central pos and radius of the given workspace
        for (int i = 0; i < 3 ; i++)
        {
            minBounds[i] = ws->getMinBound(i);
            maxBounds[i] = ws->getMaxBound(i);

            float dist = fabs(maxBounds[i] - minBounds[i]);
            pos(i) = minBounds[i] + (dist / 2);

            if (dist > maxDist) {
                maxDist = dist;
            }
        }

        float r = maxDist / 2;
        Sphere sphere;
        // min/max bounds were given in local coords
        ws->toGlobalVec(pos);
        sphere.pos = pos;
        sphere.r = r;

        return sphere;
    }

    Planner::Sphere Planner::getBoundingSphere(const VirtualRobot::ManipulationObjectPtr& object) const
    {
        VirtualRobot::BoundingBox bb = object->getBoundingBox(true);
        Eigen::Vector3f max = bb.getMax();
        Eigen::Vector3f min = bb.getMin();
        float maxDist = 0;

        for (int i = 0; i < 3; i++)
        {
            float dist = fabs(max(i) - min(i));
            if (dist > maxDist) {
                maxDist = dist;
            }
        }

        Eigen::Vector3f pos = object->getGlobalPose().block<3,1>(0,3);
        float r = maxDist / 2;
        Sphere sphere;
        sphere.pos = pos;
        sphere.r = r;

        return sphere;
    }

    Eigen::Matrix4f Planner::sampleReceiverGraspingPoseGlobal(const Eigen::Matrix4f &giverGraspingPose,
                                                              const VirtualRobot::ManipulationObjectPtr &object,
                                                              const VirtualRobot::WorkspaceRepresentationPtr &receiverWorkspace,
                                                              const VirtualRobot::ModelPtr &receiver) const
    {
        Eigen::Matrix4f receiverGraspingPose = giverGraspingPose;
        // find the eef corresponding to the given workspace's tcp
        VirtualRobot::EndEffectorPtr receiverEEF;
        for (auto &&eef : receiver->getEndEffectors())
        {
            if (eef->getTcpName() == receiverWorkspace->getTCP()->getName())
            {
                receiverEEF = eef;
                break;
            }
        }
        if (!receiverEEF)
        {
            VR_ERROR << "Could not find EEF with name '" << receiverWorkspace->getTCP()->getName() << "'. Aborting." << std::endl;
            return receiverGraspingPose;
        }

        // get a random grasp for the receiver
        VirtualRobot::GraspSetPtr graspSetReceiver = object->getGraspSet(receiver->getName(), receiverEEF->getName());
        int graspIndex = std::rand() % graspSetReceiver->getSize();
        VirtualRobot::GraspPtr graspReceiver = graspSetReceiver->getGrasp(graspIndex);

        Eigen::Matrix4f objectPose = data.giverGrasp->getObjectTargetPoseGlobal(giverGraspingPose);
        receiverGraspingPose = graspReceiver->getTcpPoseGlobal(objectPose);

        return receiverGraspingPose;
    }
}
