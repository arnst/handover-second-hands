#ifndef HANDOVER_PLANNER_H
#define HANDOVER_PLANNER_H

#include <memory>
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Workspace/WorkspaceRepresentation.h>
#include <VirtualRobot/Model/ManipulationObject.h>
#include <VirtualRobot/Grasping/Grasp.h>

#include "PoseQualityHandover.h"


namespace Handover
{
    /**
     * A planner which creates an Interaction Workspace (IWS) between two agents (giver and receiver) performing a handover task.
     * The IWS is a subspace of the giver's workspace and contains potential grasp poses (voxel) to hand over an object.
     * Just choose a voxel with a high quality value, place the giver's hand into it (with the object attached) and you can be sure
     * that the receiver agent is in range to grasp the object from the other side.
     * Both agents must be in range for this to work, i. e. standing next to eacht other.
     */
    class Planner
    {
    public:
        struct PlanningData
        {
            PlanningData() : discrStepTrans(0), discrStepRot(0), measure(VirtualRobot::PoseQualityMeasurementPtr()),
                receiverGazeOriginGlobal(Eigen::Vector3f::Zero()), receiverGazeDirection(Eigen::Vector3f::Zero()), receiverGraspSamples(1), numThreads(1)
            {}
            /** (Required) The giving model. */
            VirtualRobot::ModelPtr giver;
            /** (Required) The receiving model. */
            VirtualRobot::ModelPtr receiver;
            /** (Required) The workspace of the giving model. */
            VirtualRobot::WorkspaceRepresentationPtr giverWorkspace;
            /** (Required) The workspaces for the receiving model, e. g. one for each arm. **/
            std::vector<VirtualRobot::WorkspaceRepresentationPtr> receiverWorkspaces;
            /** (Required) The object to hand over */
            VirtualRobot::ManipulationObjectPtr object;
            /** (Required) The grasp used by the giving robot to hold the object. */
            VirtualRobot::GraspPtr giverGrasp;
            /** (Optional) if not set, then the discretization of the giverWorkspace will be used [mm]. */
            float discrStepTrans;
            /** (Optional) if not set, then the discretization of the giverWorkspace will be used [rad]. */
            float discrStepRot;
            /** (Optional) Used for the buildup of the interaction workspace. If null, then PoseQualityHandover will be used. */
            VirtualRobot::PoseQualityMeasurementPtr measure;
            /** (Optional) Used to additionally weight voxels based on the gaze direction of the receiver. If not set, then gaze won't be considered. */
            Eigen::Vector3f receiverGazeOriginGlobal;
            /** (Optional) Used to additionally weight voxels based on the gaze direction of the receiver. If not set, then gaze won't be considered. */
            Eigen::Vector3f receiverGazeDirection;
            /** (Optional) How many random grasps should be tried to grasp the object by the receiver. Default value is 1. Too many grasp samples could cause long computation times. */
            unsigned int receiverGraspSamples;
            /** (Optional) The number of threads to use. Use as many as possible since the planning task is usually quite expensive. If not set, then only 1 Thread will be used. */
            int numThreads;
        };

        Planner();

        /**
         * Sets the planning data and clones (or loads) the receiver model for each given receiver workspace and thread, i. e. this method takes relatively long (~1sec).
         * @param pData the data used to build up the interaction workspace.
         */
        void setPlanningData(const PlanningData& pData);
        /**
         * Creates an interaction workspace (= a subset of the giver's workspace) with grasp poses, that can be used by the specified giver to hand over a specified object
         * with a specified grasp. The grasp poses (i.e. the 6D-Voxels) have quality values, which are computed by the #PoseQualityHandover measure.
         * Note: both agents (i. e. giver and receiver) must be next to each other.
         * Note2: the performance of this method depends highly on the given planning data.
         * @param verbose whether to output some info or not.
         * @return The Interaction Workspace. Choose a high quality voxel and place the giver's hand into it for a good hand over.
         * */
        VirtualRobot::WorkspaceRepresentationPtr plan(bool verbose = false);
        bool validatePlanningData(PlanningData &data, bool spam);

    private:
        PlanningData data;
        std::vector< std::vector<VirtualRobot::ModelPtr> > clonedReceiversPerThread;
        std::vector< std::vector<PoseQualityHandoverPtr> > measuresPerThread;
        struct Sphere
        {
            Eigen::Vector3f pos;
            float r;
        };

        void fillInteractionWorkspace(VirtualRobot::WorkspaceRepresentationPtr &iws, std::vector<VirtualRobot::ModelPtr> &clonedReceivers,
                                      std::vector<PoseQualityHandoverPtr> &measures, const std::pair<float, float> &workload, std::mutex &mutex, int threadID, bool verbose = false);
        // returns the tcp pose of the receiver when applying a random compatible grasp to the object held by the giver grasp at specified giverGraspingPose.
        Eigen::Matrix4f sampleReceiverGraspingPoseGlobal(const Eigen::Matrix4f &giverGraspingPose,
                                                         const VirtualRobot::ManipulationObjectPtr &object,
                                                         const VirtualRobot::WorkspaceRepresentationPtr &ws,
                                                         const VirtualRobot::ModelPtr &receiver) const;
        Sphere getBoundingSphere(const VirtualRobot::ManipulationObjectPtr& object) const;
        Sphere getBoundingSphere(const VirtualRobot::WorkspaceRepresentationPtr& ws) const;
        // returns how well the given global position is visible for the receiver with the gaze properties given in the planning data.
        float getVisibility(const Eigen::Vector3f& globalPos) const;
        bool solveIK(const VirtualRobot::JointSetPtr& js, const Eigen::Matrix4f& targetPoseGlobal);
        // computes the distance vector between the static and dynamic collision models of the given workspace.
        Eigen::Vector3f getObstacleDistanceVector(const VirtualRobot::LinkSetPtr &staticColModel, const VirtualRobot::LinkSetPtr &dynamicColModel, const VirtualRobot::FramePtr &tcp) const;
        unsigned char ComputeQualityValue(unsigned char offlineEntry, unsigned char onlineEntry, float w1, float w2) const;
    };
    typedef std::shared_ptr<Planner> PlannerPtr;
}


#endif //HANDOVER_PLANNER_H
