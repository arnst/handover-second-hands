#include "Planner.h"

#include <VirtualRobot/RuntimeEnvironment.h>
#include <VirtualRobot/XML/ModelIO.h>
#include <VirtualRobot/XML/ObjectIO.h>
#include <Gui/ViewerFactory.h>
#include <VirtualRobot/Grasping/GraspSet.h>
#include <VirtualRobot/Workspace/Reachability.h>
#include <VirtualRobot/Visualization/ColorMap.h>
#include <VirtualRobot/IK/GenericIKSolver.h>

SimoxGui::ViewerInterfacePtr viewer;
VirtualRobot::ModelPtr giver;
VirtualRobot::ModelPtr receiver;
VirtualRobot::ManipulationObjectPtr object;
VirtualRobot::GraspPtr giverGrasp;
VirtualRobot::WorkspaceRepresentationPtr giverWorkspace;
std::vector<VirtualRobot::WorkspaceRepresentationPtr> receiverWorkspaces;

VirtualRobot::WorkspaceRepresentationPtr createInteractionWorkspace()
{
    Handover::PlannerPtr planner(new Handover::Planner());

    QElapsedTimer timer;
    std::cout << "[Planner] Init Planning Data...";
    timer.start();
    Handover::Planner::PlanningData data;
    data.giver = giver;
    data.receiver = receiver;
    data.giverWorkspace = giverWorkspace;
    data.receiverWorkspaces = receiverWorkspaces;
    data.object = object;
    data.giverGrasp = giverGrasp;
    data.discrStepTrans = 100;
    data.discrStepRot = 1.2f;
    data.numThreads = 8;
//    data.receiverGazeOriginGlobal = ...; // should be set for better results
//    data.receiverGazeDirection = ...; // should be set for better results

    planner->setPlanningData(data);
    std::cout << "Done! Took " << timer.elapsed() << "ms." << std::endl;

    std::cout << "[Planner] Building the Interaction Workspace..." << std::endl;
    timer.start();
    VirtualRobot::WorkspaceRepresentationPtr interactionWorkspace = planner->plan(true);
    std::cout << "[Planner] Done! Took " << timer.elapsed() << "ms." << std::endl;

    std::cout << "Interaction Workspace Status:" << std::endl;
    interactionWorkspace->print();
    return interactionWorkspace;
}

Eigen::Matrix4f sampleGoodHandoverPose(VirtualRobot::WorkspaceRepresentationPtr iws)
{
    Eigen::Matrix4f bestPose = Eigen::Matrix4f::Zero();
    unsigned char maxEntry = 0;
    for (int i = 0; i < 1000; i++)
    {
        Eigen::Matrix4f samplePose = iws->sampleCoveredPose();
        unsigned char entry = iws->getEntry(samplePose);
        if (entry > maxEntry)
        {
            bestPose = samplePose;
            maxEntry = entry;
        }
    }
    return bestPose;
}

bool loadSetup()
{
    std::string modelFile = "robots/Armar3/Armar3.xml";
    if (VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(modelFile))
    {
        giver = VirtualRobot::ModelIO::loadModel(modelFile);
        receiver = VirtualRobot::ModelIO::loadModel(modelFile);

        Eigen::Affine3f transform;
        transform = Eigen::Translation<float, 3>(1400, 0, 0) * Eigen::AngleAxisf(M_PI / 2, Eigen::Vector3f(0, 0, 1));
        giver->setGlobalPose(transform * giver->getGlobalPose());
        transform = Eigen::AngleAxisf(-M_PI / 1.5f, Eigen::Vector3f(0, 0, 1));
        receiver->setGlobalPose(transform * receiver->getGlobalPose());
    }
    else
    {
        VR_ERROR << "Could not find '" << modelFile << "'. Aborting." << std::endl;
        return false;
    }

    std::string objectFile = "objects/iv/plate.xml";
    std::string graspSetName = "GraspSet LeftHand";
    std::string graspName = "Left Grasp0";
    if (VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(objectFile))
    {
        object = VirtualRobot::ObjectIO::loadManipulationObject(objectFile);
        giverGrasp = object->getGraspSet(graspSetName)->getGrasp(graspName);
        giver->getEndEffector(giverGrasp->getEefName())->openActors();
        object->setGlobalPose(giverGrasp->getTargetPoseGlobal(giver));
        giver->getEndEffector(giverGrasp->getEefName())->closeActors(object);
    }
    else
    {
        VR_ERROR << "Could not find '" << objectFile << "'. Aborting." << std::endl;
        return false;
    }

    std::string wsFile = "workspace/armar3_leftArm_reachability_withColModels.bin"; // Note: The workspace files MUST have static and dynamic colmodels.
    if (VirtualRobot::RuntimeEnvironment::getDataFileAbsolute(wsFile))
    {
        giverWorkspace.reset(new VirtualRobot::Reachability(giver));
        giverWorkspace->load(wsFile);

        VirtualRobot::ReachabilityPtr receiverWorkspace(new VirtualRobot::Reachability(receiver));
        receiverWorkspace->load(wsFile);
        receiverWorkspaces.push_back(receiverWorkspace);
    }
    else
    {
        VR_ERROR << "Could not find '" << wsFile << "'. Aborting." << std::endl;
        return false;
    }

    return true;
}

void setupUI(QWidget &window)
{
    window.setFixedSize(800, 600);
    viewer = SimoxGui::ViewerFactory::getInstance()->createViewer(&window);
    viewer->setAntialiasing(1);
    viewer->viewAll();
    window.show();
}

void render(VirtualRobot::WorkspaceRepresentationPtr &iws)
{
    viewer->addVisualization("all", receiver->getVisualization());
    viewer->addVisualization("all", giver->getVisualization());
    giver->getEndEffector(giverGrasp->getEefName())->openActors();
    object->setGlobalPose(giverGrasp->getTargetPoseGlobal(giver));
    giver->getEndEffector(giverGrasp->getEefName())->closeActors(object);
    viewer->addVisualization("all", object->getVisualization());
//    viewer->addVisualization("all", giverWorkspace->getVisualization(VirtualRobot::ColorMapPtr(new VirtualRobot::ColorMap(VirtualRobot::ColorMap::type::eHot)), true));
//    for (auto &&ws :receiverWorkspaces)
//    {
//        viewer->addVisualization("all", ws->getVisualization(VirtualRobot::ColorMapPtr(new VirtualRobot::ColorMap(VirtualRobot::ColorMap::type::eHot)), true));
//
//    }
    viewer->addVisualization("all", iws->getVisualization(VirtualRobot::ColorMapPtr(new VirtualRobot::ColorMap(VirtualRobot::ColorMap::type::eHot)), true));
    viewer->viewAll();
}

int main(int argc, char* argv[])
{
    VirtualRobot::init(argc, argv, "Handover Test-App");
    if (!loadSetup()) return -1;

    QWidget window;
    setupUI(window);

    VirtualRobot::WorkspaceRepresentationPtr iws = createInteractionWorkspace();
    // now that we have the iws, we just choose a voxel with high quality and place the giver's hand into it.
    Eigen::Matrix4f goodHandoverPose = sampleGoodHandoverPose(iws);
    VirtualRobot::GenericIKSolverPtr ikSolver(new VirtualRobot::GenericIKSolver(giverWorkspace->getJointSet()));
    ikSolver->setupJacobian(0.2f, 50);
    ikSolver->setMaximumError(50, 0.6);
    ikSolver->solve(goodHandoverPose, VirtualRobot::IKSolver::All, 4);

    render(iws);
    return qApp->exec();
}
