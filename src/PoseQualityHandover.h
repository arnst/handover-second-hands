#ifndef HANDOVER_POSEQUALITYHANDOVER_H
#define HANDOVER_POSEQUALITYHANDOVER_H

#include <memory>


#include <VirtualRobot/IK/PoseQualityMeasurement.h>

namespace Handover
{
    class PoseQualityHandover : public virtual VirtualRobot::PoseQualityMeasurement
    {
    public:
        explicit PoseQualityHandover(const VirtualRobot::JointSetPtr &rns);
        virtual ~PoseQualityHandover() {};

        float getPoseQuality() override;

    private:
        std::vector<float> refJointValues;
        Eigen::Matrix4f refTcpPose; // global
        float maxTravelCost;
        float maxSafetyCost;

        float getSpatialErrorCost();
        float getTravelCost();
        float getMaxTravelCost();
        float getSafetyCost();
    };
    typedef std::shared_ptr<PoseQualityHandover> PoseQualityHandoverPtr;
}


#endif //HANDOVER_POSEQUALITYHANDOVER_H
